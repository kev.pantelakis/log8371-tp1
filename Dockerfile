FROM ubuntu:18.10
LABEL description="CI Docker for TP1"
LABEL maintainer="youva.chemam@polymtl.ca"

WORKDIR /build_env
ADD . /build_env/

RUN apt-get update && apt-get install -y maven default-jdk ant

RUN echo $(pwd)
RUN echo $(hostname)